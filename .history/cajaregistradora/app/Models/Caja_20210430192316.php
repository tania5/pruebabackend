<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad',
        'denominacion',
    ];

    public function validarDenominacion($denominacion)
    {
        $denominaciones = array(100000, 50000, 20000, 10000, 5000, 1000, 500, 200, 100, 50); 
        return (in_array($denominacion, $denominaciones));
    }

    public function darCambio($monto, $cantidad)
    {
        $cambio = $cantidad - $monto;
        $devuelta = array();
        $caja = Caja::where('denominacion', $cambio)->get();
        if($caja->isEmpty())
        {
            array_push($devuelta, ['cantidad'=>$total, 'denominacion'=>$denominacion]);
        }
        else
        {
            $cambio = darCambio();
        }
    }
}
