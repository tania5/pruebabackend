<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'movimiento',
        'cantidad',
        'denominacion',
    ];

    public function registrarMovimiento($movimiento, $cantidad, $denominacion)
    {
         $log = Log::create([
             'movimiento' => $movimiento,
             'cantidad' => $cantidad,
             'denominacion' => $denominacion,
             ]);
    }

    public function verLog(Request $request){
        
    }
}
