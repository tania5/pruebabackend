<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad',
        'denominacion',
    ];

    public function validarDenominacion($denominacion)
    {
        $denominaciones = array(100000, 50000, 20000, 10000, 5000, 1000, 500, 200, 100, 50); 
        return (in_array($denominacion, $denominaciones));
    }

    public function darCambio($cantidad)
    {
        $caja = Caja::where('denominacion', $cantidad)->get();

        if($caja->isEmpty())
        {
            $caja = Caja::get()->groupBy('denominacion');
            foreach($caja as $denominacion=>$collection)
            {
                foreach($collection as $i)
                {
                    
                    $total = $cantidad - $i->denominacion;
                    echo $denominacion."\n";
                    echo $total."\n";
                    if($total>0)
                    {
                        $devuelta = ['cantidad'=>'1', 'denominacion'=>$i->denominacion];
                        echo $devuelta[0];
                        $caja = new Caja();
                        $caja->darCambio($total, $devuelta);
                    }
                    else if($total == 0)
                    {
                        return $devuelta;
                    }
                    
                }
            }
        }
        else
        {
            array_push($devuelta, ['cantidad'=>'1', 'denominacion'=>$caja[0]['denominacion']]);
        }
        return $devuelta;
    }
}
