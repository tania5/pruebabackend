<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad',
        'denominacion',
    ];

    public function validarDenominacion($denominacion)
    {
        $denominaciones = array(100000, 50000, 20000, 10000, 5000, 1000, 500, 200, 100, 50); 
        return (in_array($denominacion, $denominaciones));
    }

    public function darCambio($cantidad, $devuelta)
    {
        $caja = Caja::where('denominacion', $cantidad)->get();

        if($caja->isEmpty())
        {
            $caja = Caja::get()->groupBy('denominacion');
            $data = array();
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $cantidad - $i->cantidad;

                    if($total<0)
                    {
                        darCambio($cantidad, $devuelta);
                    }
                    else
                    {

                    }
                }
                array_push($data, ['cantidad'=>$total, 'denominacion'=>$denominacion]);
            }
        }
        else
        {
            array_push($devuelta, ['cantidad'=>'1', 'denominacion'=>$denominacion]);
        }
    }
}
