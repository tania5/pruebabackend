<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');

           $movimientos = array();
           $detalle = array();
           foreach($log as $movimiento=>$collection)
           {
            echo $movimiento .'\n';
               foreach($collection as $item)
               {
     
                echo '====================== \n';
                echo 'Denomi:'. $item->denominacion.', Cantidad:' .$item->cantidad.'\n';
                $arrayTemp = array(
                    ,
                );
                $detalle = array_push($detalle, $arrayTemp);
                    
               }
              
               $movimientos[$movimiento] = $detalle;
           }


            $response = ['code'=>200, 'message'=>'OK', 'data'=>$movimientos];


           // $response = ['code'=>200, 'message'=>'OK', 'data'=>$log];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }
}
