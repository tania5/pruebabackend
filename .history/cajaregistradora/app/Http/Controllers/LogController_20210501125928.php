<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');

           $movimientos = array();
           foreach($log as $movimiento=>$collection)
            {
                $arrayTemp = array();
                $total = 0;
                foreach($collection as $i)
                {
                   array_push($arrayTemp, [$i->denominacion => $i->cantidad, 'fecha' => date($i->created_at)]);
                   
                }
                $movimientos[$movimiento] = $arrayTemp;     
            }

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$movimientos];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function consultarEstado(Request $request){
        try{
            $fecha = $request->fecha;

            $log = Log::whereTime('created_at', '=',$fecha->format('Y-m-d h:m:s'));


        }catch(\Exception $exc){
            $response = ['code'=>200, 'message'=>$exc-getMessage(), 'data'=>null];
        }
        $response = ['code'=>200, 'message'=>'OK', 'data'=>$log];

        return response()->json($response);
    }
}
