<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;
use \stdClass;

class CajaController extends Controller
{
    public function cargarBase(Request $request)
    {
        $caja = new Caja();
        if($caja->validarDenominacion($request->input('denominacion')))
        {
           $response = $this->registrarBase($request->cantidad, $request->denominacion);
        }
        else
        {
            $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            
            $denominaciones = array();
            $totalCaja = 0;
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $totalCaja += intval($denominacion)*$total;
                $denominaciones[$denominacion] = $total;
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function realizarPago(Request $request)
    {
        //Contar el total
        $montoPago = $request->monto;
        $pago = $request->pago;
        $response = array();
        $arrayPago = array();
        $totalPago = 0;
        //Datos ordenados
        foreach($pago as $denominacion => $cantidad)
        {
            $arrayPago[intval($denominacion)] = $cantidad;
        }
        krsort($arrayPago);

        $denominaciones = Caja::get()->groupBy('denominacion');

        $denominacionesDisponiblesOrdenado = array();
        foreach($denominaciones as $denominacion => $collection)
        {
            $total = 0;
            foreach($collection as $i)
            {
                $total = $total +$i->cantidad;
            }
            $denominacionesDisponiblesOrdenado[intval($denominacion)] = $total;
        }
        krsort($denominacionesDisponiblesOrdenado);

        $calculoRequeridos = $this->calcularDenominacionesNecesariasPago($arrayPago, $montoPago);
        $arrayRegresoUsuario = $calculoRequeridos->norequeridos;
        $totalPago = $this->calcularCantidadPago($calculoRequeridos->requeridos);

        $excedente = $totalPago - $montoPago;
        
        if ($excedente > 0) {            
            
            $arrayRegresoCaja = $this->calcularArrayRegreso(
                $excedente,
                $calculoRequeridos->requeridos,
                $denominacionesDisponiblesOrdenado
            );
            
            if ($arrayRegresoCaja != false) {

                foreach($arrayRegresoCaja as $denominacion => $cantidad){
                    print_r("Clave: $denominacion ; Valor: $cantidad");
                    $this->actualizarCaja($denominacion, $cantidad);
                }
                

                foreach ($arrayRegresoUsuario as $denominacion => $cantidad) {
                    if ($cantidad > 0) {
                        if (array_key_exists($denominacion, $arrayRegresoCaja)) {
                            $arrayRegresoCaja[$denominacion] += $cantidad;
                        } else {
                            $arrayRegresoCaja[$denominacion] = $cantidad;
                        }
                    }
                }
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$arrayRegresoCaja];
               
                print_r("============================== ENTRANTE \n");
                print_r($calculoRequeridos->requeridos);
            } else {

                $response = ['code'=>402, 'message'=>'No hay dinero suficiente en la caja para dar cambio.', 'data'=>null];
            }

        } else if ($excedente == 0) {
            if(count($arrayRegresoUsuario)>1){
                foreach($arrayRegresoUsuario as $denominacion => $cantidad){
                    if($cantidad > 0 ){
                        $arrayPagoSobrante[$denominacion] = $cantidad;
                        $response = ['code'=>200, 'message'=>'OK', 'data'=>$arrayPagoSobrante];
                    }
                }                  
            }
            else
            {
                $response = ['code'=>405, 'message'=>'No hay cambio. El pago es exacto.', 'data'=>null];
            }
            
        } else {
            $response = ['code'=>405, 'message'=>'El monto entregado no es suficiente para realizar el pago.', 'data'=>null];  
        }
           

        return response()->json($response);
    }


    private function actualizarCaja($cantidad, $denominacion){
        try
        {
            $registroCaja = Caja::where('denominacion', '=', $denominacion)->firstOrFail();
            print_r("=============== REGISTRO CONSULTADO\n");
            print_r($registroCaja);
        }
        catch(\Exception $exe){

        }
    }

    private function registrarBase($cantidad, $denominacion){
        try
           {
               $caja = Caja::create([
               'cantidad' => $cantidad,
               'denominacion' => $denominacion,
               ]);

               //Registrar el movimiento
               $log = new Log();
               $log->registrarMovimiento('Entrada', $cantidad, $denominacion);
               $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
           }
           catch(\Exception $ex)
           {
               $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
           }
           return $response;
    }
     /**
     * Función que calcula la cantidad de cada denominación necesarias para
     * realizar un pago
     */
    private function calcularDenominacionesNecesariasPago($arrayPago, $cantidadPagar)
    {
        $objRespuesta = new stdClass();
        $denominacionesRequeridas = array();
        $denominacionesNoRequeridas = array();

        $cantidadRestante = $cantidadPagar;

        foreach ($arrayPago as $denominacion => $cantidad) {

            $cantidadNecesaria = 0;

            if ($cantidadRestante > 0) {

                for ($cantidadtmp = 1; $cantidadtmp <= $cantidad; $cantidadtmp ++) {
                    
                    $cantidadTemporal = $denominacion * $cantidadtmp;
                    $cantidadNecesaria = $cantidadtmp;
                    if ($cantidadTemporal >= $cantidadRestante) {
                        $cantidadRestante -= $cantidadTemporal;
                        break;
                    }

                }

                $denominacionesRequeridas[$denominacion] = $cantidadNecesaria;
                $denominacionesNoRequeridas[$denominacion] = $cantidad - $cantidadNecesaria;
            } else {
                $denominacionesNoRequeridas[$denominacion] = $cantidad;
            }
        }

        $objRespuesta->requeridos = $denominacionesRequeridas;
        $objRespuesta->norequeridos = $denominacionesNoRequeridas;
        return $objRespuesta;
    }

      /**
     * Función que calcula un array de denominaciones necesarias para cubrir
     * un excedente
     */
    private function calcularArrayRegreso($excedente, $arrayPago, $denominacionesDefinidas) {
     
        $excedenteRestante = $excedente;
        $excedenteArray = array();

        foreach ($denominacionesDefinidas as $denominacion => $cantidadDenominacion) {

            if ($cantidadDenominacion > 0) {

                if ($excedenteRestante >= $denominacion) {

                    $excedenteCubiertoDenominacion = 0;
                    $cantidadDevolverDenominacion = 1;
                    for ($cantidadtmp = 1; $cantidadtmp <= $cantidadDenominacion; $cantidadtmp ++) {

                        $excedenteTemporal = $denominacion * $cantidadtmp;
                        if ($excedenteTemporal <= $excedenteRestante) {
                            $excedenteCubiertoDenominacion = $excedenteTemporal;
                            $cantidadDevolverDenominacion = $cantidadtmp;
                        } else {
                            break;
                        }
                    }
                    $excedenteRestante -= $excedenteCubiertoDenominacion;
                    $excedenteArray[$denominacion] = $cantidadDevolverDenominacion;
                }
            } 

        }

        if ($excedenteRestante == 0) {

            return $excedenteArray;

        } else {

            return false;
        }
    }

    
    /**
     * Función que calcula la cantidad de un pago
     * 
     * To do: validar que las denominaciones sean válidas
     */
    private function calcularCantidadPago($arrayPago) 
    {
        $totalPago = 0;
        foreach ($arrayPago as $denominacion => $cantidad) {
            $totalPago += $denominacion * $cantidad;
        }
        return $totalPago;
    }


}
