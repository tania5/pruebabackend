<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');
            

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$log];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }
}
