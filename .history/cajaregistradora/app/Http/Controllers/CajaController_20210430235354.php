<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;

class CajaController extends Controller
{
    public function cargarBase(Request $request)
    {
        $caja = new Caja();
        if($caja->validarDenominacion($request->input('denominacion')))
        {
            try
            {
                $caja = Caja::create([
                'cantidad' => $request->input('cantidad'),
                'denominacion' => $request->input('denominacion'),
                ]);

                //Registramos el movimiento
                $log = new Log();
                $log->registrarMovimiento('Entrada', $request->input('cantidad'), $request->input('denominacion'));
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
            }
            catch(\Exception $ex)
            {
                $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
            }
        }
        else
        {
            $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            
            $denominaciones = array();
            $totalCaja = 0;
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $totalCaja += intval($denominacion)*$total;
                $denominaciones[$denominacion] = $total;
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function realizarPago(Request $request)
    {
        try
        {
            //Contar el total
            $monto = $request->monto;
            $pago = $request->pago;
            $response = array();
            $pagoOrdenado = array();
            $totalPago = 0;
            //Datos ordenados
            foreach($pago as $denominacion => $cantidad)
            {
                $pagoOrdenado[intval($denominacion)] = $cantidad;
                $totalPago += intval($denominacion)*$cantidad;
            }
            krsort($pagoOrdenado);

            $denominaciones = Caja::get()->groupBy('denominacion');

            $denominacionesOrdenado = array();
            foreach($denominaciones as $denominacion => $collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $denominacionesOrdenado[intval($denominacion)] = $total;
            }
            krsort($denominacionesOrdenado);

            $excedente = $totalPago - $monto;

            if($excedente > 0)
            {
                //Dar regreso
                $excedenteRestante = $excedente;
                $excedenteArray = array();
                foreach($denominacionesOrdenado as $denominacion => $cantidad)
                {
                    if($cantidad > 0)
                    {
                        if($excedenteRestante >= $denominacion)
                        {
                            $excedenteCubiertoDenominacion = 0;
                            $cantidadDevolverDenominacion = 1;
                            for($cantidadtmp=1; $cantidadtmp <= $cantidad; $cantidadtmp++)
                            {
                                $excedenteTemporal = $denominacion +$cantidadtmp;
                                if($excedenteTemporal <= $excedenteRestante)
                                {
                                    $excedenteCubiertoDenominacion = $excedenteTemporal;
                                    $cantidadDevolverDenominacion = $cantidadtmp;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            $excedenteRestante -= $excedenteCubiertoDenominacion;
                            $excedenteArray[$denominacion] = $cantidadDevolverDenominacion;
                            $response = ['code'=>200, 'message'=>'OK', 'data'=>$excedenteArray];
                        }
                    }
                }
                if ($excedenteRestante == 0) {

                    $response = ['code'=>200, 'message'=>'OK', 'data'=>$excedenteArray];
                }
                else
                {
                    echo "No hay plata hpta lárguese de mi local\n";
                }
            }
            else if($excedente == 0)
            {

                echo "Con mucho gusto, lo esperamos en otra ocasión\n";
            }
            else
            {
                echo "Le hace falta plata mi ciel4, aquí no vengas a contaminar con tu pobreza\n";   
            }


        }
        catch (\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response); 
    }

     /**
     * Función que calcula la cantidad de cada denominación necesarias para
     * realizar un pago
     */
    private function calcularDenominacionesNecesariasPago($arrayPago, $cantidadPagar)
    {
        $objRespuesta = new stdClass();
        $denominacionesRequeridas = array();
        $denominacionesNoRequeridas = array();

        $cantidadRestante = $cantidadPagar;

        foreach ($arrayPago as $denominacion => $cantidad) {

            $cantidadNecesaria = 0;

            if ($cantidadRestante > 0) {

                for ($cantidadtmp = 1; $cantidadtmp <= $cantidad; $cantidadtmp ++) {
                    
                    $cantidadTemporal = $denominacion * $cantidadtmp;
                    $cantidadNecesaria = $cantidadtmp;
                    if ($cantidadTemporal >= $cantidadRestante) {
                        $cantidadRestante -= $cantidadTemporal;
                        break;
                    }

                }

                $denominacionesRequeridas[$denominacion] = $cantidadNecesaria;
                $denominacionesNoRequeridas[$denominacion] = $cantidad - $cantidadNecesaria;
            } else {
                $denominacionesNoRequeridas[$denominacion] = $cantidad;
            }
        }

        $objRespuesta->requeridos = $denominacionesRequeridas;
        $objRespuesta->norequeridos = $denominacionesNoRequeridas;
        return $objRespuesta;
    }

      /**
     * Función que calcula un array de denominaciones necesarias para cubrir
     * un excedente
     */
    private function calcularArrayRegreso($excedente, $arrayPago, $denominacionesDefinidas) {
     
        $excedenteRestante = $excedente;
        $excedenteArray = array();

        foreach ($denominacionesDefinidas as $denominacion => $cantidadDenominacion) {

            if ($cantidadDenominacion > 0) {

                if ($excedenteRestante >= $denominacion) {

                    $excedenteCubiertoDenominacion = 0;
                    $cantidadDevolverDenominacion = 1;
                    for ($cantidadtmp = 1; $cantidadtmp <= $cantidadDenominacion; $cantidadtmp ++) {

                        $excedenteTemporal = $denominacion * $cantidadtmp;
                        if ($excedenteTemporal <= $excedenteRestante) {
                            $excedenteCubiertoDenominacion = $excedenteTemporal;
                            $cantidadDevolverDenominacion = $cantidadtmp;
                        } else {
                            break;
                        }
                    }
                    $excedenteRestante -= $excedenteCubiertoDenominacion;
                    $excedenteArray[$denominacion] = $cantidadDevolverDenominacion;
                }
            } 

        }

        if ($excedenteRestante == 0) {

            return $excedenteArray;

        } else {

            return false;
        }
    }

}
