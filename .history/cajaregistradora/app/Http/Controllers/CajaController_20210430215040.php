<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;

class CajaController extends Controller
{
    public function cargarBase(Request $request)
    {
        $caja = new Caja();
        if($caja->validarDenominacion($request->input('denominacion')))
        {
            try
            {
                $caja = Caja::create([
                'cantidad' => $request->input('cantidad'),
                'denominacion' => $request->input('denominacion'),
                ]);

                //Registramos el movimiento
                $log = new Log();
                $log->registrarMovimiento('Entrada', $request->input('cantidad'), $request->input('denominacion'));
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
            }
            catch(\Exception $ex)
            {
                $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
            }
        }
        else
        {
            $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    

    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            
            $denominaciones = array();
            $totalCaja = 0;
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $totalCaja += intval($denominacion)*$total;
                $denominaciones[$denominacion] = $total;
                //$data, ['cantidad'=>$total, 'denominacion'=>$denominacion]);
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function realizarPago(Request $request)
    {
        try
        {
            //Contar el total
            $monto = $request->monto;
            $pago = $request->pago;
            
            $pagoOrdenado = array();
            //Datos ordenados
            foreach($pago as $denominacion => $cantidad)
            {
                $pagoOrdenado[inval($denominacion)] = $cantidad;
            }


            $cantidad = 0;
            $caja = new Caja();
            $denominacionesValidas = false;
            for ($i = 0; $i < count($pago); $i++) 
            {
                if($caja->validarDenominacion($pago[$i]['denominacion']))
                {
                    $denominacionesValidas = true;
                }
                else
                {
                    $denominacionesValidas = false;
                }
            }

            if($denominacionesValidas)
            {
                
                //Dar devuelta
                $cambio = $cantidad - $monto;

                $devuelta = $caja->darCambio($cambio);
               
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$devuelta[0]];

                //CUando se da cambio se guarda el dinero
                for ($i = 0; $i < count($pago); $i++) 
                {
                    $caja = Caja::create([
                        'cantidad' => $pago[$i]['cantidad'],
                        'denominacion' => $pago[$i]['denominacion'],
                        ]);
        
                    //Registramos el movimiento
                    $log = new Log();
                    $log->registrarMovimiento('Entrada', $pago[$i]['cantidad'], $pago[$i]['denominacion']);
                    $cantidad = $cantidad + $pago[$i]['cantidad']*$pago[$i]['denominacion'];
                }
            }
            else
            {
                $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
            }
        }
        catch (\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response); 
    }


    

}
