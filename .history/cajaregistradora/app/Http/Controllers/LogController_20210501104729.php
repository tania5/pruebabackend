<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogController extends Controller
{
    public function verLog(Request $request){
        $log = Log::get()->groupBy('denominacion');
            
        $denominaciones = array();
        $totalCaja = 0;
        foreach($caja as $denominacion=>$collection)
        {
            $total = 0;
            foreach($collection as $i)
            {
                $total = $total +$i->cantidad;
            }
            $totalCaja += intval($denominacion)*$total;
            $denominaciones[$denominacion] = $total;
        }
        $data = array(
            'total' => $totalCaja,
            'detalle' => $denominaciones
        );

        $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
    }
    catch(\Exception $ex)
    {
        $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
    }
    return response()->json($response);
    }
}
