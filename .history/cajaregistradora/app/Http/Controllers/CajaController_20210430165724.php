<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;

class CajaController extends Controller
{
    public function cargarBase(Request $request)
    {
        if(validarDenominacion($request->input('denominacion')))
        {
            try
            {
                $caja = Caja::create([
                'cantidad' => $request->input('cantidad'),
                'denominacion' => $request->input('denominacion'),
                ]);

                //Registramos el movimiento
                $log = new Log();
                $log->registrarMovimiento($request->input('cantidad'), $request->input('denominacion'));
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
            }
            catch(\Exception $ex)
            {
                $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
            }
        }
        else
        {
            $response = ['code'=>200, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    

    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            $data = array();
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                array_push($data, ['cantidad'=>$total, 'denominacion'=>$denominacion]);
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function realizarPago(Request $request)
    {
        try
        {
            //Contar el total
            $monto = $request->input('monto');
            foreach($request->input('pago') as $valor)
            {
                echo $valor;
            }
        }
        catch (\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }

    }


    public function validarDenominacion($denominacion)
    {
        $denominaciones = array(100000, 50000, 20000, 10000, 5000, 1000, 500, 200, 100, 50); 
        return (in_array($request->input('denominacion'), $denominaciones));
    }

}
