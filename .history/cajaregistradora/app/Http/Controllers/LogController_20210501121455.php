<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');

           $movimientos = array();
            $totalCaja = 0;
            $arrayTemp = array();
            foreach($log as $movimiento=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $arrayTemp = array_push($arrayTemp, new array(
                        'cantidad' => $i->cantidad))
                   
                }
                print_r($arrayTemp);
                $movimientos[$movimiento] = $arrayTemp;
               
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $movimientos
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$movimientos];


           // $response = ['code'=>200, 'message'=>'OK', 'data'=>$log];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }
}
