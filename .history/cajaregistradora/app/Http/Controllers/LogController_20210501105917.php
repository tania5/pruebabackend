<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');

            $movimientos = array();
            $totalCaja = 0;
            foreach($log as $movimiento=>$collection)
            {
                $movimientos[$movimiento] = $collection;
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $movimientos,
                
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];


           // $response = ['code'=>200, 'message'=>'OK', 'data'=>$log];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }
}
