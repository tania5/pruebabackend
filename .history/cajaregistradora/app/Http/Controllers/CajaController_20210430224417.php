<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;

class CajaController extends Controller
{
    public function cargarBase(Request $request)
    {
        $caja = new Caja();
        if($caja->validarDenominacion($request->input('denominacion')))
        {
            try
            {
                $caja = Caja::create([
                'cantidad' => $request->input('cantidad'),
                'denominacion' => $request->input('denominacion'),
                ]);

                //Registramos el movimiento
                $log = new Log();
                $log->registrarMovimiento('Entrada', $request->input('cantidad'), $request->input('denominacion'));
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
            }
            catch(\Exception $ex)
            {
                $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
            }
        }
        else
        {
            $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    

    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            
            $denominaciones = array();
            $totalCaja = 0;
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $totalCaja += intval($denominacion)*$total;
                $denominaciones[$denominacion] = $total;
                //$data, ['cantidad'=>$total, 'denominacion'=>$denominacion]);
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function realizarPago(Request $request)
    {
        try
        {
            //Contar el total
            $monto = $request->monto;
            $pago = $request->pago;
            
            $pagoOrdenado = array();
            $totalPago = 0;
            //Datos ordenados
            foreach($pago as $denominacion => $cantidad)
            {
                $pagoOrdenado[inval($denominacion)] = $cantidad;
                $totalPago += inval($denominacion)*$cantidad;
            }
            krsort($pagoOrdenado);

            $caja = Caja::get()->groupBy('denominacion');
            $denominaciones = array_keys($caja);
            $denominacionesOrdenado = array();
            foreach($denominaciones as $denominacion)
            {
                $denominacionesOrdenado[inval($denominacion)] = $cantidad;
            }
            krsort($denominacionesOrdenado);

            $excedente = $totalPago - $monto;

            if($excedente > 0)
            {
                //Dar regreso
                $excedenteRestante = $excedente;
                $excedente = array();
                foreach($denominacionesOrdenado as $denominacion => $cantidad)
                {
                    if($cantidad > 0)
                    {
                        if($excedenteRestante >= $denominacion)
                        {
                            $excedenteCubiertoDenominacion = 0;
                            $cantidadDevolverDenominacion = 1;
                            for($cantidadtmp=1; $cantidadtmp <= $cantidad; $cantidadtmp++)
                            {
                                $excedenteTemporal = $denominacion +$cantidadtmp;
                                if($excedenteTemporal )
                                {

                                }
                            }
                    
                            

                        }
                    }
                }
            }
            else if($excedente == 0)
            {
                //No hay regreso
            }
            else
            {
                //No alcanza
            }


        }
        catch (\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response); 
    }


    

}
