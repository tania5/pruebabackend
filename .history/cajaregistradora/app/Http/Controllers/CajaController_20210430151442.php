<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;

class CajaController extends Controller
{
    public function cargarBase(Request $request)
    {
        $denominaciones = array(100000, 50000, 20000, 10000, 5000, 1000, 500, 200, 100, 50); 
        if(in_array($request->input('denominacion'), $denominaciones))
        {
            try
            {
                $caja = Caja::create([
                'cantidad' => $request->input('cantidad'),
                'denominacion' => $request->input('denominacion'),
                ]);

                //Registramos el movimiento
                $log = new Log();
                $log->registrarMovimiento($request->input('cantidad'), $request->input('denominacion'));
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
            }
            catch(\Exception $ex)
            {
                $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
            }
        }
        else
        {
            $response = ['code'=>200, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function estadoCaja(Request $request)
    {
        try
        {
        //$caja = Caja::get()->groupBy('cantidad');
           $caja = Caja::get()->groupBy('denominacion');

           foreach($caja as $item=>$denominacion)
           {
               
           }

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }
}
