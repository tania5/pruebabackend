<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');

           $movimientos = array();
           $detalle = array();
           foreach($log as $movimiento=>$collection)
           {

                echo $movimiento ."\n";
               foreach($collection as $item)
               {
     
                echo "====================== \n";
                echo 'Denomi:'. $item->denominacion.', Cantidad:' .$item->cantidad." Fecha: $item->created_at \n";
                $arrayTemp = ['denominacion' => $item->denominacion,
                    'cantidad' => $item->cantidad,
                    'fecha' => $item->created_at];
                $detalle = array_merge($detalle, $arrayTemp);
               }
               print_r("====================================== DETALLE \n");
               print_r($detalle);
                $detalle[$movimiento] = $detalle;  
               
               $movimientos[$movimiento] = $detalle;
           }


            $response = ['code'=>200, 'message'=>'OK', 'data'=>$detalle];


           // $response = ['code'=>200, 'message'=>'OK', 'data'=>$log];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }
}
