<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function verLog(Request $request){
       try{
           $log = Log::get()->groupBy('movimiento');

           $movimientos = array();
           foreach($log as $movimiento=>$collection)
            {
                $arrayTemp = array();
                $total = 0;
                foreach($collection as $i)
                {
                   array_push($arrayTemp, [$i->denominacion => $i->cantidad, 'fecha' => date($i->created_at)]);
                   
                }
                $movimientos[$movimiento] = $arrayTemp;     
            }

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$movimientos];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    public function consultarEstado(Request $request){
        try{
            $fecha = $request->fecha;

            $log = Log::where('created_at', '=',$fecha)->orderBy('denominacion')->get();

            $denominaciones = array();
            $totalCaja = 0;
            foreach($log as $denominacion=>$i)
            {
                $totalCaja += intval($i->denominacion)*$i->cantidad;
                if (array_key_exists($denominacion, $denominaciones)) {
                    $arrayRegresoCaja[$denominacion] += $cantidad;
                } else {
                    $arrayRegresoCaja[$denominacion] = $cantidad;
                }
                $denominaciones[$denominacion] = array(
                    $i->denominacion => $i->cantidad
                );
                
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];

        }catch(\Exception $ex){
            $response = ['code'=>200, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        

        return response()->json($response);
    }
}
