<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;
use \stdClass;

class CajaController extends Controller
{
     /**
     * Función que añade una base de dinero en 
     * la caja registradora
     */
    public function cargarBase(Request $request)
    {
        $caja = new Caja();
        if($caja->validarDenominacion($request->denominacion))
        {
           $response = $this->registrarBase($request->cantidad, $request->denominacion);
        }
        else
        {
            $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
        }
        return response()->json($response);
    }

    /**
     * Función que elimina todo el dinero de la caja 
     * registradora
     */
    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    /**
     * Función que consulta el estado actual de la caja registradora 
     */
    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            
            $denominaciones = array();
            $totalCaja = 0;
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $totalCaja += intval($denominacion)*$total;
                $denominaciones[$denominacion] = $total;
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
        }
        return response()->json($response);
    }

    /**
     * Función que registra el pago de una compra,
     * actualiza el estado de la caja contemplando el
     * dinero entrante/saliente y retorna el cambio más óptimo
     */
    public function realizarPago(Request $request)
    {
        $montoPago = $request->monto;
        $pago = $request->pago;
        $response = array();
        $arrayPago = array();
        $totalPago = 0;
        $log = new Log();
        $caja = new Caja();
        
        foreach($pago as $denominacion => $cantidad)
        {
            if($caja->validarDenominacion($denominacion)){
                $arrayPago[intval($denominacion)] = $cantidad;
            }
            else
            {
                $response = ['code'=>200, 'message'=>"La denominación $denominacion no es válida.", 'data'=>null];
                return response()->json($response);
            }
        }
        krsort($arrayPago);

        $denominaciones = Caja::get()->groupBy('denominacion');
        $denominacionesDisponiblesOrdenado = array();
        foreach($denominaciones as $denominacion => $collection)
        {
            $total = 0;
            foreach($collection as $i)
            {
                $total = $total +$i->cantidad;
            }
            $denominacionesDisponiblesOrdenado[intval($denominacion)] = $total;
        }
        krsort($denominacionesDisponiblesOrdenado);

        //Se tranza solamente con la cantidad necesesaria
        $calculoRequeridos = $this->calcularDenominacionesNecesariasPago($arrayPago, $montoPago);
        $arrayRegresoUsuario = $calculoRequeridos->norequeridos;
        $totalPago = $this->calcularCantidadPago($calculoRequeridos->requeridos);

        $excedente = $totalPago - $montoPago;
        if ($excedente > 0) {            
            
            $arrayRegresoCaja = $this->calcularArrayRegreso(
                $excedente,
                $calculoRequeridos->requeridos,
                $denominacionesDisponiblesOrdenado
            );
            
            if ($arrayRegresoCaja != false) {
                foreach ($arrayRegresoUsuario as $denominacion => $cantidad) {
                    if ($cantidad > 0) {
                        if (array_key_exists($denominacion, $arrayRegresoCaja)) {
                            $arrayRegresoCaja[$denominacion] += $cantidad;
                        } else {
                            $arrayRegresoCaja[$denominacion] = $cantidad;
                        }
                    }
                }
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$arrayRegresoCaja];
            } else {

                $response = ['code'=>402, 'message'=>'No hay dinero suficiente en la caja para dar cambio.', 'data'=>null];
            }

        } else if ($excedente == 0) {
            if(count($arrayRegresoUsuario)>1){
                foreach($arrayRegresoUsuario as $denominacion => $cantidad){
                    if($cantidad > 0 ){
                        $arrayPagoSobrante[$denominacion] = $cantidad;
                        $response = ['code'=>200, 'message'=>'OK', 'data'=>$arrayPagoSobrante];
                    }
                }                  
            }
            else
            {
                $response = ['code'=>405, 'message'=>'No hay cambio. El pago es exacto.', 'data'=>null];
            } 
        } else {
            $response = ['code'=>405, 'message'=>'El pago ingresado no es suficiente para cubrir el monto.', 'data'=>null];  
        }

        //Disminuir la cantidad de la denominación saliente
        foreach($arrayRegresoCaja as $denominacion => $cantidad){
            $this->actualizarCaja($denominacion, $cantidad);
            $log->registrarMovimiento('Salida', $cantidad, $denominacion);
        }

         //Registrar la cantidad y denominación entrante
         foreach($calculoRequeridos->requeridos as $denominacion => $cantidad){
             print_r("Denominacion: $denominacion ,  Cantidad: $cantidad\n");
            $this->registrarBase($cantidad, $denominacion);
        }
        
        return response()->json($response);
    }

    /**
     * Función que actualiza la cantidad de una denominación
     */
    private function actualizarCaja($denominacion, $cantidad){
        $registroCaja = Caja::where('denominacion', $denominacion)->first();
        $caja = Caja::find($registroCaja->id);
        print_r($caja->cantidad);
        if($registroCaja->cantidad > 1)
        {
            $caja->cantidad = $caja->cantidad - $cantidad;
            $caja->save();
        }
        else
        {
            $caja->delete();
        }
    }

    /**
     * Función que actualiza la cantidad de una denominación en específico
     */
    private function registrarBase($cantidad, $denominacion){
        try
           {
               $caja = Caja::create([
               'cantidad' => $cantidad,
               'denominacion' => $denominacion,
               ]);

               //Registrar el movimiento
               $log = new Log();
               $log->registrarMovimiento('Entrada', $cantidad, $denominacion);
               $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
           }
           catch(\Exception $ex)
           {
               $response = ['code'=>500, 'message'=>$ex->getMessage(), 'data'=>null];
           }
           return $response;
    }

    /**
     * Función que calcula la cantidad de cada denominación necesarias para
     * realizar un pago
     */
    private function calcularDenominacionesNecesariasPago($arrayPago, $cantidadPagar)
    {
        $objRespuesta = new stdClass();
        $denominacionesRequeridas = array();
        $denominacionesNoRequeridas = array();

        $cantidadRestante = $cantidadPagar;

        foreach ($arrayPago as $denominacion => $cantidad) {

            $cantidadNecesaria = 0;

            if ($cantidadRestante > 0) {

                for ($cantidadtmp = 1; $cantidadtmp <= $cantidad; $cantidadtmp ++) {
                    
                    $cantidadTemporal = $denominacion * $cantidadtmp;
                    $cantidadNecesaria = $cantidadtmp;
                    if ($cantidadTemporal >= $cantidadRestante) {
                        $cantidadRestante -= $cantidadTemporal;
                        break;
                    }

                }

                $denominacionesRequeridas[$denominacion] = $cantidadNecesaria;
                $denominacionesNoRequeridas[$denominacion] = $cantidad - $cantidadNecesaria;
            } else {
                $denominacionesNoRequeridas[$denominacion] = $cantidad;
            }
        }

        $objRespuesta->requeridos = $denominacionesRequeridas;
        $objRespuesta->norequeridos = $denominacionesNoRequeridas;
        return $objRespuesta;
    }

     /**
     * Función que calcula un array de denominaciones necesarias para cubrir
     * un excedente
     */
    private function calcularArrayRegreso($excedente, $arrayPago, $denominacionesDefinidas) {
     
        $excedenteRestante = $excedente;
        $excedenteArray = array();

        foreach ($denominacionesDefinidas as $denominacion => $cantidadDenominacion) {

            if ($cantidadDenominacion > 0) {

                if ($excedenteRestante >= $denominacion) {
                    $excedenteCubiertoDenominacion = 0;
                    $cantidadDevolverDenominacion = 1;
                    for ($cantidadtmp = 1; $cantidadtmp <= $cantidadDenominacion; $cantidadtmp ++) {

                        $excedenteTemporal = $denominacion * $cantidadtmp;
                        if ($excedenteTemporal <= $excedenteRestante) {
                            $excedenteCubiertoDenominacion = $excedenteTemporal;
                            $cantidadDevolverDenominacion = $cantidadtmp;
                        } else {
                            break;
                        }
                    }
                    $excedenteRestante -= $excedenteCubiertoDenominacion;
                    $excedenteArray[$denominacion] = $cantidadDevolverDenominacion;
                }
            } 

        }

        if ($excedenteRestante == 0) {
            return $excedenteArray;
        } else {
            return false;
        }
    }
    
    /**
     * Función que calcula la cantidad de un pago 
     * To do: validar que las denominaciones sean válidas
     */
    private function calcularCantidadPago($arrayPago) 
    {
        $totalPago = 0;
        $caja = new Caja;
        foreach ($arrayPago as $denominacion => $cantidad) {
            $totalPago += $denominacion * $cantidad;
        }
        return $totalPago;
    }
}
