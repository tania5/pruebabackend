<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caja;
use App\Http\Controllers\LogController;
use App\Models\Log;
use \stdClass;
use Illuminate\Support\Facades\Validator;

class CajaController extends Controller
{
     /**
     * Función que añade una base de dinero en 
     * la caja registradora
     */
    public function cargarBase(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'denominacion' => 'required',
            'cantidad' => 'required',
            ]);
    
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }

        $caja = new Caja();
        if($caja->validarDenominacion($request->denominacion))
        {
           $response = $this->registrarBase($request->cantidad, $request->denominacion);
        }
        else
        {
            $response = ['code'=>401, 'message'=>'Denomidación inválida', 'data'=>null];
            http_response_code(401);
        }
        return response()->json($response, 400);
    }

    /**
     * Función que elimina todo el dinero de la caja 
     * registradora
     */
    public function vaciarCaja(Request $request)
    {
        try
        {
            $collection = Caja::get();
            foreach($collection as $item)
            {
                $caja = Caja::find($item->id);
                $caja->delete();
            }
            $response = ['code'=>200, 'message'=>'OK', 'data'=>null];
        }
        catch (\Exception $ex){
            $response = ['code'=>500, 'message'=>'Error interno del servidor.', 'data'=>null];
        }
        return response()->json($response);
    }

    /**
     * Función que consulta el estado actual de la caja registradora 
     */
    public function estadoCaja(Request $request)
    {
        try
        {
            $caja = Caja::get()->groupBy('denominacion');
            
            $denominaciones = array();
            $totalCaja = 0;
            foreach($caja as $denominacion=>$collection)
            {
                $total = 0;
                foreach($collection as $i)
                {
                    $total = $total +$i->cantidad;
                }
                $totalCaja += intval($denominacion)*$total;
                $denominaciones[$denominacion] = $total;
            }
            $data = array(
                'total' => $totalCaja,
                'detalle' => $denominaciones
            );

            $response = ['code'=>200, 'message'=>'OK', 'data'=>$data];
        }
        catch(\Exception $ex)
        {
            $response = ['code'=>500, 'message'=>'Error interno del servidor.', 'data'=>null];
        }
        return response()->json($response);
    }

    /**
     * Función que registra el pago de una compra,
     * actualiza el estado de la caja contemplando el
     * dinero entrante/saliente y retorna el cambio más óptimo
     */
    public function realizarPago(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'monto' => 'required',
            'pago' => 'required',
            ]);
    
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }

        $montoPago = $request->monto;
        $pago = $request->pago;
        $response = array();
        $arrayPago = array();
        $arrayRegresoCaja = array();
        $arrayActualizar = array();
        $totalPago = 0;
        $log = new Log();
        $caja = new Caja();
        
        if($montoPago<50){
            $response = ['code'=>401, 'message'=>"El monto $montoPago no es válido.", 'data'=>null];
            return response()->json($response);
        }

        foreach($pago as $denominacion => $cantidad)
        {
            if($caja->validarDenominacion($denominacion)){
                $arrayPago[intval($denominacion)] = $cantidad;
            }
            else
            {
                $response = ['code'=>200, 'message'=>"La denominación $denominacion no es válida.", 'data'=>null];
                return response()->json($response);
            }
        }
        krsort($arrayPago);

        $denominaciones = Caja::get()->groupBy('denominacion');
        $denominacionesDisponiblesOrdenado = array();
        foreach($denominaciones as $denominacion => $collection)
        {
            $total = 0;
            foreach($collection as $i)
            {
                $total = $total +$i->cantidad;
            }
            $denominacionesDisponiblesOrdenado[intval($denominacion)] = $total;
        }
        krsort($denominacionesDisponiblesOrdenado);

        //Se transa solamente con la cantidad necesesaria
        $calculoRequeridos = $this->calcularDenominacionesNecesariasPago($arrayPago, $montoPago);
        $arrayRegresoUsuario = $calculoRequeridos->norequeridos;
        $totalPago = $this->calcularCantidadPago($calculoRequeridos->requeridos);

        $excedente = $totalPago - $montoPago;
        if ($excedente > 0) {            
            
            $arrayRegresoCaja = $this->calcularArrayRegreso(
                $excedente,
                $calculoRequeridos->requeridos,
                $denominacionesDisponiblesOrdenado
            );
             
            if ($arrayRegresoCaja != false) {
                $arrayActualizar = $arrayRegresoCaja;
                //Disminuir la cantidad de la denominación saliente
                foreach($arrayActualizar as $denominacion => $cantidad){
                    $this->actualizarCaja($denominacion, $cantidad);
                    $log->registrarMovimiento('Salida', $cantidad, $denominacion);
                }
                foreach ($arrayRegresoUsuario as $denominacion => $cantidad) {
                    if ($cantidad > 0) {
                        if (array_key_exists($denominacion, $arrayRegresoCaja)) {
                            $arrayRegresoCaja[$denominacion] += $cantidad;
                        } else {
                            $arrayRegresoCaja[$denominacion] = $cantidad;
                        }
                    }
                }
                $response = ['code'=>200, 'message'=>'OK', 'data'=>$arrayRegresoCaja];
            } else {

                $response = ['code'=>402, 'message'=>'No hay dinero suficiente en la caja para dar cambio.', 'data'=>null];
            }

        } else if ($excedente == 0) {
            if(count($arrayRegresoUsuario)>1){    
                foreach($arrayRegresoUsuario as $denominacion => $cantidad){
                    if($cantidad > 0 ){
                        $arrayPagoSobrante[$denominacion] = $cantidad;
                        $response = ['code'=>201, 'message'=>'OK', 'data'=>$arrayPagoSobrante];
                    }
                }      
            }
            else
            {
                $response = ['code'=>405, 'message'=>'No hay cambio. El pago es exacto.', 'data'=>null];
            } 
        } else {
            $response = ['code'=>405, 'message'=>'El pago ingresado no es suficiente para cubrir el monto.', 'data'=>null];  
        }
         //Registrar la cantidad y denominación entrante
         foreach($calculoRequeridos->requeridos as $denominacion => $cantidad){
            $this->registrarBase($cantidad, $denominacion);
        }
        return response()->json($response);
    }

    /**
     * Función que actualiza la cantidad de una denominación
     */
    private function actualizarCaja($denominacion, $cantidad){
        $registroCaja = Caja::where('denominacion', $denominacion)->first();
        $caja = Caja::find($registroCaja->id);
        if($registroCaja->cantidad > 1)
        {
            $caja->cantidad = $caja->cantidad - $cantidad;
            $caja->save();
        }
        else
        {
            $caja->delete();
        }
    }

    /**
     * Función que actualiza la cantidad de una denominación en específico
     */
    private function registrarBase($cantidad, $denominacion){
        try
           {
               $caja = Caja::create([
               'cantidad' => $cantidad,
               'denominacion' => $denominacion,
               ]);

               //Registrar el movimiento
               $log = new Log();
               $log->registrarMovimiento('Entrada', $cantidad, $denominacion);
               $response = ['code'=>200, 'message'=>'OK', 'data'=>$caja];
           }
           catch(\Exception $ex)
           {
               $response = ['code'=>500, 'message'=>'Error interno del servidor.', 'data'=>null];
           }
           return $response;
    }

    /**
     * Función que calcula la cantidad de cada denominación necesarias para
     * realizar un pago
     */
    private function calcularDenominacionesNecesariasPago($arrayPago, $cantidadPagar)
    {
        $objRespuesta = new stdClass();
        $denominacionesRequeridas = array();
        $denominacionesNoRequeridas = array();

        $cantidadRestante = $cantidadPagar;

        foreach ($arrayPago as $denominacion => $cantidad) {

            $cantidadNecesaria = 0;
            if ($cantidadRestante > 0) {
                for ($cantidadtmp = 1; $cantidadtmp <= $cantidad; $cantidadtmp ++) {  
                    $cantidadTemporal = $denominacion * $cantidadtmp;
                    $cantidadNecesaria = $cantidadtmp;
                    if ($cantidadTemporal >= $cantidadRestante) {
                        $cantidadRestante -= $cantidadTemporal;
                        break;
                    }
                }

                $denominacionesRequeridas[$denominacion] = $cantidadNecesaria;
                $denominacionesNoRequeridas[$denominacion] = $cantidad - $cantidadNecesaria;
            } else {
                $denominacionesNoRequeridas[$denominacion] = $cantidad;
            }
        }

        $objRespuesta->requeridos = $denominacionesRequeridas;
        $objRespuesta->norequeridos = $denominacionesNoRequeridas;
        return $objRespuesta;
    }

     /**
     * Función que calcula un array de denominaciones necesarias para cubrir
     * un excedente
     */
    private function calcularArrayRegreso($excedente, $arrayPago, $denominacionesDefinidas) {
     
        $excedenteRestante = $excedente;
        $excedenteArray = array();

        foreach ($denominacionesDefinidas as $denominacion => $cantidadDenominacion) {
            if ($cantidadDenominacion > 0) {
                if ($excedenteRestante >= $denominacion) {
                    $excedenteCubiertoDenominacion = 0;
                    $cantidadDevolverDenominacion = 1;
                    for ($cantidadtmp = 1; $cantidadtmp <= $cantidadDenominacion; $cantidadtmp ++) {
                        $excedenteTemporal = $denominacion * $cantidadtmp;
                        if ($excedenteTemporal <= $excedenteRestante) {
                            $excedenteCubiertoDenominacion = $excedenteTemporal;
                            $cantidadDevolverDenominacion = $cantidadtmp;
                        } else {
                            break;
                        }
                    }
                    $excedenteRestante -= $excedenteCubiertoDenominacion;
                    $excedenteArray[$denominacion] = $cantidadDevolverDenominacion;
                }
            } 
        }
        if ($excedenteRestante == 0) {
            return $excedenteArray;
        } else {
            return false;
        }
    }
    
    /**
     * Función que calcula el total del pago
     */
    private function calcularCantidadPago($arrayPago) 
    {
        $totalPago = 0;
        $caja = new Caja;
        foreach ($arrayPago as $denominacion => $cantidad) {
            $totalPago += $denominacion * $cantidad;
        }
        return $totalPago;
    }
}
