<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Caja;
use JWTAuth;
use Illuminate\Support\Facades\Route;

class CargarBaseTest extends TestCase
{
    /** @test */
    function cargar_base_denominacion_valida()
    {
        $formData = [
            'cantidad' => '10',
            'denominacion' => '10000'
        ];

        $this->post('caja.cargarBase', $formData)
             ->assertStatus(200);
    }
}