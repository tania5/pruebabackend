<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CargarBaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    function testCargarBaseCajaRegistradora()
    {
        $this->json('post', 'api/caja')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' =>[
                        'cantidad' => '10',
                        'denominacion' => '10000'
                    ]
                ]
            );

        $response = $this->get('/');

        $response->assertStatus(200);
    }
}