<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Caja;
use JWTAuth;
use Illuminate\Support\Facades\Route;

class CargarBaseTest extends TestCase
{
    /** @test */
    function cargar_base_denominacion_valida()
    {
        $formData = [
            'cantidad' => '10',
            'denominacion' => '10000'
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => config('app.token')
        ])->json('POST', 'api/caja', $formData)
             ->assertStatus(200);
    }

    /** @test */
    function cargar_base_denominacion_invalida()
    {
        $formData = [
            'cantidad' => '5',
            'denominacion' => '00'
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => config('app.token')
        ])->json('POST', 'api/caja', $formData);
        $response->assertStatus(400);
    }

    /** @test */
    function cargar_base_cantidad_valida()
    {
        $formData = [
            'cantidad' => '10',
            'denominacion' => '1000'
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => config('app.token')
        ])->json('POST', 'api/caja', $formData);
        $response->assertStatus(200);
    }

     /** @test */
     function cargar_base_cantidad_invalida()
     {
         $formData = [
             'cantidad' => '0',
             'denominacion' => '1000'
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => config('app.token')
         ])->json('POST', 'api/caja', $formData);
         $response->assertStatus(400);
     }

     /** @test */
     function cargar_base_sin_parametros()
     {
         $formData = [
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => config('app.token')
         ])->json('POST', 'api/caja', $formData);
         $response->assertStatus(400);
     }

     /** @test */
     function cargar_base_sin_denominacion()
     {
         $formData = [
             'cantidad' => '1'
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => config('app.token')
         ])->json('POST', 'api/caja', $formData);
         $response->assertStatus(400);
     }
}