<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Caja;
class CargarBaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    function testCargarBaseCajaRegistradora()
    {
        $caja = Caja::factory()->create([
            'cantidad' => '1',
            'denominacion' => '10000'
        ]);

        $this->json('POST', 'api/caja')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    "caja" => [
                        'cantidad',
                        'denominacion',
                    ]
                ]
            );
    }
}