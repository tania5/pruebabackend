<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RealizarPagoTest extends TestCase
{
    /** @test */
    function registrar_pago_con_el_monto_justo()
    {
        $formData = [
            'monto' => '10000',
            'pago' => 
                [
                    '10000' => 1
                ]
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxNTQzNiwiZXhwIjoxNjE5OTE5MDM2LCJuYmYiOjE2MTk5MTU0MzYsImp0aSI6ImFzUHlwNlFIa2M3QjZXNzkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.sW45dh8_oW-kJcVfEjvO3glHtNbia6zeYmcfv30VAho'
        ])->json('POST', 'api/caja/pago', $formData)
             ->assertStatus(200);
    }

     /** @test */
     function registrar_pago_con_monto_superior()
     {
         $formData = [
             'monto' => '15000',
             'pago' => 
                 [
                     '10000' => 2,
                     '5000' => 1
                 ]
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxNTQzNiwiZXhwIjoxNjE5OTE5MDM2LCJuYmYiOjE2MTk5MTU0MzYsImp0aSI6ImFzUHlwNlFIa2M3QjZXNzkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.sW45dh8_oW-kJcVfEjvO3glHtNbia6zeYmcfv30VAho'
         ])->json('POST', 'api/caja/pago', $formData)
              ->assertStatus(200);
     }

     /** @test */
     function registrar_pago_con_denominacion_invalida()
     {
         $formData = [
             'monto' => '15000',
             'pago' => 
                 [
                     '15000' => 1
                 ]
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxOTY5NywiZXhwIjoxNjE5OTIzMjk3LCJuYmYiOjE2MTk5MTk2OTcsImp0aSI6IlprU1JZTVdpZVFEZEZRNUwiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.3_wjDaTMeOaeEb7P3KI47E05R57mIPtgoVtQtExlVHs'
         ])->json('POST', 'api/caja/pago', $formData)
              ->assertStatus(400);
     }

      /** @test */
      function registrar_pago_con_cantidad_invalida()
      {
          $formData = [
              'monto' => '15000',
              'pago' => 
                  [
                      '15000' => 0
                  ]
          ];
  
          $response = $this->withHeaders([
              'Accept' => 'application/json',
              'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxOTY5NywiZXhwIjoxNjE5OTIzMjk3LCJuYmYiOjE2MTk5MTk2OTcsImp0aSI6IlprU1JZTVdpZVFEZEZRNUwiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.3_wjDaTMeOaeEb7P3KI47E05R57mIPtgoVtQtExlVHs'
          ])->json('POST', 'api/caja/pago', $formData)
               ->assertStatus(400);
      }

      /** @test */
      function registrar_pago_por_monto_invalido()
      {
          $formData = [
              'monto' => '00',
              'pago' => 
                  [
                      '20000' => 1
                  ]
          ];
  
          $response = $this->withHeaders([
              'Accept' => 'application/json',
              'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxOTY5NywiZXhwIjoxNjE5OTIzMjk3LCJuYmYiOjE2MTk5MTk2OTcsImp0aSI6IlprU1JZTVdpZVFEZEZRNUwiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.3_wjDaTMeOaeEb7P3KI47E05R57mIPtgoVtQtExlVHs'
          ])->json('POST', 'api/caja/pago', $formData)
               ->assertStatus(400);
      }

      /** @test */
      function registrar_pago_con_parametros_errados())
      {
          $formData = [
              'monto' => '00',
              'pago' => 
                  [
                  ]
          ];
  
          $response = $this->withHeaders([
              'Accept' => 'application/json',
              'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxOTY5NywiZXhwIjoxNjE5OTIzMjk3LCJuYmYiOjE2MTk5MTk2OTcsImp0aSI6IlprU1JZTVdpZVFEZEZRNUwiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.3_wjDaTMeOaeEb7P3KI47E05R57mIPtgoVtQtExlVHs'
          ])->json('POST', 'api/caja/pago', $formData)
               ->assertStatus(400);
      }
}

