<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RealizarPagoTest extends TestCase
{
    /** @test */
    function registrar_pago_con_el_monto_justo()
    {
        $formData = [
            'monto' => '10000',
            'pago' => 
                [
                    '10000' => 1
                ]
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => config('app.token')
        ])->json('POST', 'api/caja/pago', $formData)
             ->assertStatus(200);
    }

     /** @test */
     function registrar_pago_con_monto_superior()
     {
         $formData = [
             'monto' => '15000',
             'pago' => 
                 [
                     '10000' => 2,
                     '5000' => 1
                 ]
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => config('app.token')
         ])->json('POST', 'api/caja/pago', $formData)
              ->assertStatus(200);
     }

     /** @test */
     function registrar_pago_con_denominacion_invalida()
     {
         $formData = [
             'monto' => '15000',
             'pago' => 
                 [
                     '15000' => 1
                 ]
         ];
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => config('app.token')
         ])->json('POST', 'api/caja/pago', $formData)
              ->assertStatus(400);
     }

      /** @test */
      function registrar_pago_con_cantidad_invalida()
      {
          $formData = [
              'monto' => '15000',
              'pago' => 
                  [
                      '15000' => 0
                  ]
          ];
  
          $response = $this->withHeaders([
              'Accept' => 'application/json',
              'Authorization' => config('app.token')
          ])->json('POST', 'api/caja/pago', $formData)
               ->assertStatus(400);
      }

      /** @test */
      function registrar_pago_por_monto_invalido()
      {
          $formData = [
              'monto' => '00',
              'pago' => 
                  [
                      '20000' => 1
                  ]
          ];
  
          $response = $this->withHeaders([
              'Accept' => 'application/json',
              'Authorization' => config('app.token')
          ])->json('POST', 'api/caja/pago', $formData)
               ->assertStatus(400);
      }

      /** @test */
      function registrar_pago_con_parametros_errados()
      {
          $formData = [
              'monto' => '00',
              'pago' => 
                  [
                  ]
          ];
  
          $response = $this->withHeaders([
              'Accept' => 'application/json',
              'Authorization' => config('app.token')
          ])->json('POST', 'api/caja/pago', $formData)
               ->assertStatus(400);
      }
}