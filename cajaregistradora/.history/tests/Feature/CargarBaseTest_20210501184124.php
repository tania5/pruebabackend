<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Caja;
class CargarBaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    function testCargarBaseCajaRegistradora()
    {

        $data = [
            "cantidad" => "10",
            "denominacion" => "10000"
        ];

        $this->json('POST', 'api/caja', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson(
                [
                    "caja" => [
                        "cantidad" => "10",
                        "denominacion" => "10000",
                    ],
                    "message" => "create successfully"
                ]
            );
    }
}