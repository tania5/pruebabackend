<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Caja;
use JWTAuth;

class CargarBaseTest extends TestCase
{
    /** @test */
    function cargar_base_denominacion_valida()
    {
        $user = User::first();

        $token = JwAuth::fromUser($user);
        

        $data = [
            "cantidad" => "10",
            "denominacion" => "10000"
        ];

        $this->json('POST', 'api/caja', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson(
                [
                    "caja" => [
                        "cantidad" => "10",
                        "denominacion" => "10000",
                    ],
                    "message" => "create successfully"
                ]
            );
    }
}