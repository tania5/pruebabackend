<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CargarBaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    function testCargarBaseCajaRegistradora()
    {
        $this->json('post', 'api/caja')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'cantidad' => '10',
                    'denominacion' => '10000'
                ]
            );
    }
}