<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VaciarCajaTest extends TestCase
{
     /** @test */
     function eliminar_registros_de_la_caja()
     {
 
         $response = $this->withHeaders([
             'Accept' => 'application/json',
             'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYxOTkxNTQzNiwiZXhwIjoxNjE5OTE5MDM2LCJuYmYiOjE2MTk5MTU0MzYsImp0aSI6ImFzUHlwNlFIa2M3QjZXNzkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.sW45dh8_oW-kJcVfEjvO3glHtNbia6zeYmcfv30VAho'
         ])->json('DELETE', 'api/caja')
              ->assertStatus(200);
     }
}
